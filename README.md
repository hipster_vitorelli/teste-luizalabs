## Descrição do projeto
O teste foi desenvolvido utilizando um layout enviado com uma feature de consulta de CEP.
### Premissas
- Utilizar a API aberta da VIACEP para consultar dados como o endereço, bairro, cidade e estado.
- Criar a aplicação sendo Single Page.
- Manter um código limpo e organizado
- Disponibilizar a aplicação em um servidor público.

### Desenvolvimento

A arquitetura da aplicação foi desenhada pensando em como utilizar a featura de maneira cotidiana, não sendo apenas uma consulta a algum cep, mas trazer sentido ao uso no dia a dia.

Crei uma breve navegação que simula um ambiente micro de ecommerce onde temos uma vitrine com listagem de alguns produtos e um carrinho de compras.
Podemos adicionar, remover e alterar a quantidade dos itens na aplicação. Também existe uma consulta a uma api desenvolvida em NodeJs com 3 rotas, produtos, produto por id e estoque por id.

Durante as movimentações dos produtos(adição, alterar quantidade e remover) contamos com uma consulta de estoque que caso, já possua o produto no carrinho, a aplicação não permite prosseguir com a inclusão de mais uma unidade.

Usei **REACT, REDUX, SAGA, ~~*REACTOTRON*~~** em cima de uma arquitetura **FLUX** para extrair o máximo de performance, ponto único de verdade, transição de dados entre componentes e etc...

**REACTOTRON** para mapear tudo que acontece na aplicação, mapeamento das Actions e retornos.


## Instalação

Baixe ou clone o projeto e após isso acesse o terminal dentro da sua pasta.

Baixe as dependências do Projeto.

    yarn
Rodando em modo de Desenvolvimento

    yarn start-one
Rodando em modo de produção com Deploy

    yarn start
Rodando em modo de Build

    yarn build
    
### Dependências e versões do package.json
| LIB | VERSION |
|--|--|
|@testing-library/react  | ^9.3.2 |
|polished|^3.4.2|
|@testing-library/user-event  | ^7.1.2 |
|path|^0.12.7|
|axios  | ^0.19.2 |
|immer|^5.3.2|
|express  | ^4.17.1 |
|history|^4.10.1|
|google-maps-react  | ^2.0.2 |
|@testing-library/jest-dom  | ^4.2.4 |
|react  | ^16.12.0 |
|react-delay-render  | ^0.1.2 |
|react-dom  | ^16.12.0 |
|react-geocode  | ^0.2.1 |
|react-google-maps  | ^9.4.5 |
|react-icons  | ^3.8.0 |
|react-input-mask  | ^2.0.4 |
|react-redux  | ^7.1.3 |
|react-router-dom  | ^3.3.0 |
|react-scripts  | ^2.0.4 |
|react-toastify  | ^5.5.0 |
|reactotron-react-js  | ^3.3.7 |
|reactotron-redux  | ^3.1.2 |
|reactotron-redux-saga  | ^4.2.3 |
|redux  | ^4.0.5 |
|redux-localstore  | ^0.4.0 |
|redux-persist  | ^6.0.0 |
|redux-saga  | ^1.1.3 |
|styled-components  | ^5.0.0 |
|sweetalert2  | ^9.7.0 |