import { createGlobalStyle } from "styled-components";
import Background from "../assets/images/background.svg";
import "react-toastify/dist/ReactToastify.css";

export default createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Roboto&display=swap');

  *{
    margin: 0px;
    padding: 0px;
    outline: 0px;
    box-sizing: border-box;
  }

  body{
    background: #f2f2f2 url(${Background}) no-repeat center top;
    -webkit-font-smoothing: antialiased;
  }

  body, input, button {
    font: 14px Roboto, sans-serif;
  }

  #root {
    max-width: 1280px;
    margin: 0px auto;
    padding: 0 20px 50px;
  }

  button {
    cursor: pointer;
  }

  .swal2-popup.sem-estoque.swal2-icon-error.swal2-show {
    .swal2-icon.swal2-error{
      color: #2686ff !important;
    }
    .swal2-icon.swal2-error [class^=swal2-x-mark-line]{
      background-color: #2686ff !important;
      color: #2686ff !important;
    }
    .swal2-icon.swal2-error{
      color: #2686ff !important;
      border-color: #2686ff !important;
    }
    h2#swal2-title {
      text-transform: uppercase;
    }

    div#swal2-content {
      max-width: 300px;
      margin: 0px auto !important;
      text-align: center;
    }
  }
`;
