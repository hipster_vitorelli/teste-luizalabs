import axios from "axios";

const api = axios.create({
  baseURL: "https://back-end-luiza.grupovitorelli.com.br/"
});

export default api;
