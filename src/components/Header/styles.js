import styled from "styled-components";
import { Link } from "react-router-dom";

export const Container = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 10px 0px;
  img {
    max-width: 150px;

    @media (max-width: 380px) {
      max-width: 100px;
    }
  }
`;

export const Cart = styled(Link)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  text-decoration: none;
  transition: opacity 0.2s;
  &:hover {
    opacity: 0.7;
  }
  div {
    text-align: right;
    margin-right: 10px;
    strong {
      display: block;
      color: #fff;
    }
    span {
      font-size: 12px;
      color: #f3f3f3;
    }
  }

  @media (max-width: 380px) {
    svg {
      width: 28px;
      height: 28px;
    }
  }
`;
