import styled from "styled-components";

export const Container = styled.section`
  input {
    height: 40px;
    width: 100%;
    border: 1px solid #2686ff;
    padding: 0px 12px;
    font-weight: bold;
    &::placeholder {
      color: #2686ff;
      opacity: 1; /* Firefox */
    }

    &:-ms-input-placeholder {
      color: #2686ff;
    }

    &::-ms-input-placeholder {
      color: #2686ff;
    }
  }

  label {
    display: flex;
    align-itens: center;
  }
  h3 {
    color: #737475;
  }
  button {
    width: 260px;
    border: 0;
    background: #2686ff;
    color: #fff;
    text-transform: uppercase;
    font-size: 13px;
  }

  .endereco {
    width: 100%;
    background: #f7f7f7;
    margin: 20px 0;
    padding: 20px 15px;
    border-radius: 8px;
    h3 {
      font-weight: bold;
      font-size: 23px;
    }

    h4 {
      font-weight: 100;
      strong {
        font-size: 12px;
      }
    }
  }
  .mapaSite {
    position: relative;
    height: 363px;
  }
  .frete .frete {
    display: flex;
    align-items: center;
    justify-content: center;
    background: #afdcb1;
    color: #fff;
    text-transform: uppercase;
    font-size: 13px;
  }

  .frete .frete svg {
    margin-right: 10px;
  }

  .frete {
    margin-top: 15px;
  }
`;
