import styled from "styled-components";
import { darken } from "polished";

export const Container = styled.div`
  padding: 30px;
  background: #fff;
  border-radius: 4px;
  font-family: Roboto, sans-serif;
  display: flex;
  @media (max-width: 1050px) {
    flex-direction: column;
  }
  @media (max-width: 450px) {
    padding: 10px;
  }

  .overflow {
    max-height: 500px;
    overflow-y: scroll;
    padding-right: 5px;

    @media (max-width: 1050px) {
      max-height: initial;
      overflow-y: initial;
    }

    &::-webkit-scrollbar {
      width: 3px;
    }

    &::-webkit-scrollbar-track {
      background: #fff;
    }

    &::-webkit-scrollbar-thumb {
      background: ${darken(0.06, "#2686ff")};
    }

    &::-webkit-scrollbar-thumb:hover {
      background: #2686ff;
    }
  }

  section {
    width: 100%;
  }

  section.mapa {
    max-width: 380px;
    margin-left: 20px;
    padding-left: 16px;
    border-left: 1px solid #f4f4f4;
    @media (max-width: 1050px) {
      max-width: 100%;
      margin-left: 0px;
      padding-left: 0px;
      border-left: 0px;
      border-top: 1px solid #ececec;
      margin-top: 20px;
      padding-top: 20px;

      .mapa-content > div > div > div {
        max-width: 100% !important;
      }
    }
  }

  footer {
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    padding-top: 30px;
    width: 100%;
    @media (max-width: 700px) {
      flex-direction: column;
      & > button {
        order: 2;
        text-align: center;
        justify-content: center;
        width: 100%;
      }
      & > div {
        order: 1;
        text-align: center;
        justify-content: center;
        width: 100%;
      }
    }

    button {
      background: #2686ff;
      border: 0;
      padding: 12px 22px;
      display: flex;
      align-items: center;
      color: #fff;
      text-transform: uppercase;
      border-radius: 4px;
      transition: background 0.2s;
      font-weight: bold;

      @media (max-width: 700px) {
        padding: 14px 22px !important;
        margin-top: 20px;
      }
      svg {
        margin-right: 5px;
      }
      &:hover {
        background: ${darken(0.03, "#2686ff")};
      }
    }
  }
`;
export const ProductTable = styled.table`
  width: 100%;

  tr {
    border-bottom: 1px solid #ccc;
    @media (max-width: 700px) {
      border-bottom: 1px solid #ccc;
      display: flex;
      flex-direction: column;
      border: 2px solid #2786ff;
      border-radius: 5px;
      position: relative;
      padding: 10px;
    }
  }

  thead {
    border-bottom: 1px solid #ccc;
    @media (max-width: 700px) {
      display: none;
    }
  }

  thead th {
    color: #999;
    text-align: left;
    padding: 12px;
  }

  tbody {
    @media (max-width: 700px) {
      display: grid;
      grid-template-columns: repeat(1, 1fr);
      grid-gap: 5px;

      td:last-child {
        position: absolute;
        right: 10px;
        top: 10px;
        border: none;
      }
      td:nth-of-type(2) {
        border: none;
        position: absolute;
        right: 10px;
        top: 50px;
      }
      td:nth-of-type(4) {
        display: none;
      }
      td:nth-of-type(3) {
        display: flex;
        align-items: center;
        justify-content: center;
        border: none;
      }
    }
    @media (max-width: 450px) {
      td:nth-of-type(2) {
        border: none;
        position: initial;
        right: initial;
        top: initial;
        text-align: center;
      }
      td:nth-of-type(1) {
        display: flex;
        align-items: center;
        justify-content: center;
        border: none;
      }
    }
  }

  tbody td {
    padding: 12px;
    border-bottom: 1px solid #eee;
  }

  img {
    max-width: 100px;
    width: 100%;
    max-height: 100px;
    vertical-align: middle;
  }

  strong {
    display: block;
    color: #333;
  }

  span {
    display: inline-block;
    margin-top: 5px;
    font-size: 18px;
    font-weight: bold;
    line-height: 10px;
    padding: 7px;
    background: #e6e6e6;
    border-radius: 40px;
  }

  div {
    display: flex;
    align-items: center;

    input {
      border: 1px solid #ddd;
      border-radius: 4px;
      color: #666;
      padding: 6px;
      width: 50px;
      height: 40px;
      text-align: center;
    }
  }

  button {
    background: none;
    border: 0;
    padding: 6px;
  }
`;
export const Total = styled.div`
  display: flex;
  align-items: baseline;

  span {
    font-size: 14px;
  }

  strong {
    font-weight: bold;
    font-size: 28px;
    margin-left: 5px;
  }
`;

export const MapSection = styled.div``;
export const CartEmpty = styled.div`
  .container {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    max-width: 500px;
    margin: 20px auto;
    text-align: center;
    color: #464646;

    > a {
      display: block;
      margin-top: 20px;
      padding: 12px 22px;
      text-transform: uppercase;
      background: #2686ff;
      text-decoration: none;
      border-radius: 4px;
      color: #fff;
    }
  }
`;
