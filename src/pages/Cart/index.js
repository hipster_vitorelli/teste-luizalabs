import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {
  Container,
  ProductTable,
  Total,
  MapSection,
  CartEmpty
} from "./styles";
import {
  MdDelete,
  MdAddCircleOutline,
  MdRemoveCircleOutline,
  MdShoppingCart,
  MdSentimentDissatisfied
} from "react-icons/md";
import { formatPrice } from "../../util/format";
import * as cartAction from "../../store/modules/cart/actions";
import Swal from "sweetalert2";
import Mapa from "../../components/Mapa/index";

function Cart({ cart, total, removeToCart, updateAmountRequest }) {
  function increment(product) {
    updateAmountRequest(product.id, product.amount + 1);
  }

  function decrement(product) {
    updateAmountRequest(product.id, product.amount - 1);
  }

  function FinishClick() {
    Swal.fire({
      title: "Hey, paramos por aqui",
      text: "Foi um grande prazer poder participar deste processo seletivo.",
      icon: "success",
      showConfirmButton: false,
      timer: 6000,
      customClass: "sem-estoque",
      showCloseButton: true
    });
  }

  return (
    <Container>
      <section className="cart-page">
        {cart.length !== 0 ? (
          <div>
            <div className="overflow">
              <ProductTable>
                <thead>
                  <tr>
                    <th />
                    <th>PRODUTO</th>
                    <th>QTD</th>
                    <th>SUBTOTAL</th>
                    <th />
                  </tr>
                </thead>
                <tbody>
                  {cart.map(product => (
                    <tr key={product.id}>
                      <td>
                        <img src={product.image} alt={product.title} />
                      </td>
                      <td>
                        <strong>{product.title}</strong>
                        <span>{product.priceFormatted}</span>
                      </td>
                      <td>
                        <div>
                          <button
                            type="button"
                            onClick={() => decrement(product)}
                          >
                            <MdRemoveCircleOutline size={20} color="#2686ff" />
                          </button>
                          <input
                            type="number"
                            value={product.amount}
                            readOnly
                          />
                          <button
                            type="button"
                            onClick={() => increment(product)}
                          >
                            <MdAddCircleOutline size={20} color="#2686ff" />
                          </button>
                        </div>
                      </td>
                      <td>
                        <strong>{product.subtotal}</strong>
                      </td>
                      <td>
                        <button
                          type="button"
                          onClick={() => removeToCart(product.id)}
                        >
                          <MdDelete size={20} color="#2686ff" />
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </ProductTable>
            </div>
            <footer>
              <button type="button" onClick={() => FinishClick()}>
                <MdShoppingCart size={22} color="#fff" />
                Finalizar Compra
              </button>
              <Total>
                <span>TOTAL</span>
                <strong>{total}</strong>
              </Total>
            </footer>
          </div>
        ) : (
          <CartEmpty>
            <div className="container">
              <MdSentimentDissatisfied size={45} />
              <h1>Seu carrinho está vazio!</h1>
              <p>
                Não perca tempo e adicione um produto agora em seu carrinho e
                aproveite todas promoções da semana
              </p>
              <Link to="/">Ver promoções</Link>
            </div>
          </CartEmpty>
        )}
      </section>
      <section className="mapa">
        <MapSection>
          <Mapa />
        </MapSection>
      </section>
    </Container>
  );
}

const mapStateToProps = state => ({
  cart: state.cart.map(product => ({
    ...product,
    subtotal: formatPrice(product.price * product.amount)
  })),
  total: formatPrice(
    state.cart.reduce((total, product) => {
      return total + product.price * product.amount;
    }, 0)
  )
});

const mapDispatchToProps = dispatch => bindActionCreators(cartAction, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
